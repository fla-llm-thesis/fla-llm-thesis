# Dokumenti

Končni dokumenti so v PDF obliki – ime datoteke vključuje naslov, stanje/tip dokumenta v oklepajih in pa fizični format dokumenta v oglatih oklepajih (npr. `FLA – novi izzivi (dispozicija diplomske naloge)[A4].pdf`).

Dokument z imenom `diplomska.pdf` je vedno zadnja (zgenerirana) delovna verzija.

# Izvorna koda

Ta diplomska naloga je pisana v modernem tipografskem program(skem jezik)u, imenovanem [ConTeXt][].

Za pravilno generiranje te naloge, poleg ConTeXt paketa, potrebujete še moj [lastni modul za pravilno citiranje in slog po okusu Pravne Fakultete Univerze v Ljubljani][pful]. Stabilna verzija modula pa je pripeta kot podmodul tega repozitorija (čeprav se to ne vidi na prvi pogled).

Vso potrebno kodo se torej dobi preko [Git][] tako:

	git clone --recursive git@gitlab.com:fla-llm-thesis/fla-llm-thesis.git

# Generiranje PDF

Da bi zgenerirali nov PDF potrebujete nameščen ConTeXt ali iz [njihovega lastnega repozitorija][ConTeXt] ali pa (priporočljivo) kar celotno [TeX Live distribucijo][tl].

Nato je vse kar potrebujete storiti zagnati naslednji ukaz:

	context diplomska.tex


# Licenca

Diplomska naloga je objavljena pod licenco [CC-BY 4.0 International][cc-by]. To velja tako za tiskano različico, kot tudi to v tem repozitoriju.

[ConTeXt]: http://wiki.contextgarden.net
[tl]: http://www.tug.org/texlive
[pful]: https://gitlab.com/context-modules/pful
[Git]: http://git-scm.com
[cc-by]: https://creativecommons.org/licenses/by/4.0/
